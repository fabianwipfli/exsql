package ch.wipfli.exsql.frontend.util;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Consumer;

import ch.wipfli.exsql.frontend.App;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ControlUtils {

    public static void fileChooserOpen(Stage stage, String fileDescription, String fileExtension, String title, Consumer<File> consumer) {
        final FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(fileDescription, fileExtension);
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setTitle(title);
        final File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            consumer.accept(file);
        }
    }

    public static void fileChooserSave(Stage stage, String fileDescription, String fileExtension, String title, Consumer<File> consumer) {
        final FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(fileDescription, fileExtension);
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setTitle(title);
        final File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            consumer.accept(file);
        }
    }

    public static Optional<FXMLLoader> createWindow(String fxml, String title) {
        final String icon = "icon.png";
        Parent component;

        try {
            final FXMLLoader loader = new FXMLLoader(App.class.getResource(fxml));
            component = loader.load();
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UTILITY);
            stage.getIcons().add(new Image(App.class.getResourceAsStream(icon)));
            stage.setTitle(title);
            stage.setAlwaysOnTop(true);
            stage.setResizable(false);
            stage.setScene(new Scene(component));
            return Optional.of(loader);
        } catch (IOException e) {
            return Optional.empty();
        }
    }


    public static Stage getStage(FXMLLoader loader) {
        return (Stage) ((AnchorPane)loader.getRoot()).getScene().getWindow();
    }
}
