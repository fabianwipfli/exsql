package ch.wipfli.exsql.frontend.controller;

import ch.wipfli.exsql.frontend.App;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AboutController {

    private final static Logger LOGGER = Logger.getGlobal();

    public Label dateLabel;
    public Label versionLabel;
    public Label authorLabel;
    public Label nameLabel;
    public ImageView iconImageView;

    @FXML
    private void initialize() {
        String name = null;
        String version = null;
        String author = null;
        String date = null;
        try {
            Enumeration<URL> resources = getClass().getClassLoader()
                    .getResources("META-INF/MANIFEST.MF");
            while (resources.hasMoreElements()) {
                final Manifest manifest = new Manifest(resources.nextElement().openStream());
                final Attributes attr = manifest.getMainAttributes();
                name = attr.getValue("appName");
                version = attr.getValue("version");
                author = attr.getValue("vendor");
                date = attr.getValue("date");
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "", e);
        }

        nameLabel.setText(name != null ? name : "SmartSQL_Test");
        versionLabel.setText(version != null ? version : "Test");
        authorLabel.setText(author != null ? author : "Fabian Wipfli");
        dateLabel.setText(date != null ? author : LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        iconImageView.setImage(new Image(App.class.getResourceAsStream("icon.png")));
    }
}
