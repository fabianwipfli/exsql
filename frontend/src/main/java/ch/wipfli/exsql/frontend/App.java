package ch.wipfli.exsql.frontend;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static BorderPane root = new BorderPane();


    @Override
    public void start(Stage stage) throws IOException {
        Parent component = loadFXML("app.fxml");
        stage.setTitle("SmartSQL");
        stage.getIcons().add(new Image(App.class.getResourceAsStream("icon.png")));

        //Component
        root.setCenter(component);
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        root.setCenter(loadFXML(fxml));
        scene.setRoot(root);
    }

    private static Parent loadFXML(String fxml) throws IOException {
        final FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        Application.launch();
    }

}
