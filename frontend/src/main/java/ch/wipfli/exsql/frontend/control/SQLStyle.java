package ch.wipfli.exsql.frontend.control;

import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.model.StyleSpansBuilder;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SQLStyle extends CodeArea {
    private static final String[] KEYWORDS = new String[]{
            "SELECT"
            , "TOP"
            , "FROM"
            , "DISTINCT"
            , "LIMIT"
            , "WHERE"
            , "AND"
            , "OR"
            , "IN"
            , "NOT"
            , "LIKE"
            , "EXISTS"
            , "GROUP BY"
            , "HAVING"
            , "ORDER BY"
            , "DESC"
            , "ASC", "FULL OUTER JOIN"
            , "JOIN"
            , "INNER JOIN"
            , "LEFT JOIN"
            , "RIGHT JOIN"
            , "OUTER JOIN"
            , "UNION"
            , "UNION ALL"};

    private static final String KEYWORD_PATTERN = "\\b(" + String.join("|", KEYWORDS) + ")\\b";
    private static final String PAREN_PATTERN = "\\(|\\)";
    private static final String BRACE_PATTERN = "\\{|\\}";
    private static final String BRACKET_PATTERN = "\\[|\\]";
    private static final String SEMICOLON_PATTERN = "\\;";
    private static final String STRING_PATTERN = "\"([^\"\\\\]|\\\\.)*\"";
    private static final String COMMENT_PATTERN = "//[^\n]*" + "|" + "/\\*(.|\\R)*?\\*/";

    private static final Pattern PATTERN = Pattern.compile(
            "(?<KEYWORD>" + KEYWORD_PATTERN + ")"
                    + "|(?<PAREN>" + PAREN_PATTERN + ")"
                    + "|(?<BRACE>" + BRACE_PATTERN + ")"
                    + "|(?<BRACKET>" + BRACKET_PATTERN + ")"
                    + "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
                    + "|(?<STRING>" + STRING_PATTERN + ")"
                    + "|(?<COMMENT>" + COMMENT_PATTERN + ")"
    );

    public SQLStyle(AutoCompleteCodeArea codeArea) {
        codeArea.getStylesheets().add(SQLStyle.class.getResource("sql-keywords.css").toExternalForm());
        codeArea.setOnKeyPressed((event) -> applyHighlighting(codeArea));
        codeArea.onApplyAutocomplete((event) -> applyHighlighting(codeArea));
    }

    public void applyHighlighting(AutoCompleteCodeArea codeArea) {
        Matcher matcher = PATTERN.matcher(codeArea.getText().toUpperCase());
        int lastKwEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();
        while (matcher.find()) {
            String styleClass =
                    matcher.group("KEYWORD") != null ? "keyword" :
                            matcher.group("PAREN") != null ? "paren" :
                                    matcher.group("BRACE") != null ? "brace" :
                                            matcher.group("BRACKET") != null ? "bracket" :
                                                    matcher.group("SEMICOLON") != null ? "semicolon" :
                                                            matcher.group("STRING") != null ? "string" :
                                                                    matcher.group("COMMENT") != null ? "comment" :
                                                                            null; /* never happens */
            assert styleClass != null;
            spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), codeArea.getText().length() - lastKwEnd);
        //Apply to textarea
        codeArea.setStyleSpans(0, spansBuilder.create());
    }

}
