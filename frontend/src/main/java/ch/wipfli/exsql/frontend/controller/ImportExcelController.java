package ch.wipfli.exsql.frontend.controller;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

public class ImportExcelController {

    public TabPane tabPane;
    private List<TextField> textFieldList = new ArrayList<>();
    private int[] headerPosition = new int[0];

    @FXML
    private void initialize() {

    }

    public void initPane(String[] sheets)
    {
        for(String name : sheets) {
            tabPane.getTabs().add(createTab(name));
        }
    }

    private Tab createTab(String sheet) {
        final Tab tab = new Tab(sheet);

        final AnchorPane pane = new AnchorPane();
        pane.setPrefHeight(180);
        pane.setPrefWidth(200);

        final VBox vbox = new VBox();
        vbox.setPrefHeight(184);
        vbox.setPrefWidth(219);
        vbox.setPadding(new Insets(30, 80, 20, 80));

        final Label label = new Label("Header Position (Row)");

        final TextField textField = new TextField();
        textField.setText("1");
        textField.setOnKeyPressed(event -> {
            try {
                Integer.parseInt(event.getText());
            }
            catch(NumberFormatException e)
            {
                textField.setText("");
            }
        });
        textField.setPrefHeight(25);
        textField.setPrefWidth(163);
        textFieldList.add(textField);

        tab.setContent(vbox);
        vbox.getChildren().add(label);
        vbox.getChildren().add(textField);

        return tab;
    }

    public void onOk() {
        headerPosition = new int[textFieldList.size()];
        int position = 0;
        for (TextField field:textFieldList) {
            headerPosition[position] = Integer.parseInt(field.getText());
            position++;
        }
        tabPane.getScene().getWindow().hide();
    }

    public int[] getHeaderPosition() {
        return headerPosition;
    }
}
