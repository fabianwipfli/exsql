package ch.wipfli.exsql.frontend.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.FileChooser;

import java.io.File;

public class ImportCsvController {
    private String path = null;
    private String encoding = "UTF-8";
    private String delimiter = ",";

    public javafx.scene.control.TextField pathTextfield;
    public javafx.scene.control.TextField encodingTextfield;
    public javafx.scene.control.TextField delimiterTextfield;

    @FXML
    private void initialize() {
        encodingTextfield.setText(encoding);
        delimiterTextfield.setText(delimiter);
    }

    public void onChooseFile(ActionEvent actionEvent) {
        final String title = "Open CSV";
        final String fileExtension = "*.csv";
        final String fileDescription = "CSV";
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(fileDescription, fileExtension);
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setTitle(title);
        final File file = fileChooser.showOpenDialog(pathTextfield.getScene().getWindow());
        if (file != null) {
            pathTextfield.setText(file.getAbsolutePath());
        }
    }

    public void onOk(ActionEvent actionEvent) {
        path = pathTextfield.getText();
        encoding = encodingTextfield.getText();
        delimiter = delimiterTextfield.getText();
        pathTextfield.getScene().getWindow().hide();
    }


    public String getPath() {
        return path;
    }

    public String getEncoding() {
        return encoding;
    }

    public String getDelimitter() {
        return delimiter;
    }
}
