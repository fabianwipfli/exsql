package ch.wipfli.exsql.frontend.controller;

import static ch.wipfli.exsql.frontend.util.ControlUtils.*;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

import ch.wipfli.exsql.backend.ApplicationService;
import ch.wipfli.exsql.backend.ApplicationServiceImpl;
import ch.wipfli.exsql.backend.ValueModel;
import ch.wipfli.exsql.frontend.control.AutoCompleteCodeArea;
import ch.wipfli.exsql.frontend.control.SQLIntellisense;
import ch.wipfli.exsql.frontend.control.SQLStyle;
import ch.wipfli.exsql.frontend.util.ControlUtils;
import ch.wipfli.exsql.frontend.util.CustomLogger;
import ch.wipfli.exsql.frontend.util.TableUtils;
import ch.wipfli.exsql.frontend.util.Theme;
import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

import javafx.stage.Stage;

public class AppController {

    private final static Logger LOGGER = Logger.getGlobal();

    private List<List<ValueModel>> outputList = new ArrayList<>();

    public Button sqlRunButton;
    public Menu themeMenu;
    public Menu fileMenu;
    public TableView<ObservableList<ValueModel>> outputTable;
    public TabPane importTabPane;
    public TextArea sqlOutput;
    public BorderPane main;
    public ProgressBar progressBar;
    public AutoCompleteCodeArea sqlTextArea;

    private ApplicationService applicationService = new ApplicationServiceImpl();

    @FXML
    private void initialize() {
        CustomLogger.init(sqlOutput);
        new SQLIntellisense(importTabPane, sqlTextArea);
        new SQLStyle(sqlTextArea);
    }

    public void onExportExcel(ActionEvent actionEvent) {
        final String fileDescription = "Excel File";
        final String fileExtension = "*.xlsx";
        final String title = "Open Excel";
        final String exceptionText = "Can not export csv";

        fileChooserSave((Stage) main.getScene().getWindow(), fileDescription, fileExtension, title, file -> {
            CompletableFuture.runAsync(() -> {
                onProgress();
                try {
                    applicationService.exportExcel(outputList, file);
                } catch (Exception e) {
                    LOGGER.log(Level.SEVERE, exceptionText, e);
                }
                onProgressStop();
            });
        });
    }

    public void onExportCSV(ActionEvent actionEvent) {
        final String fxml = "importCsv.fxml";
        final String title = "Export CSV";
        final String exceptionText = "Can not export csv";

        //create new window
        ControlUtils.createWindow(fxml, title).ifPresent((fxmlLoader -> {
            final Stage stage = ControlUtils.getStage(fxmlLoader);
            final ImportCsvController controller = fxmlLoader.getController();

            //if on window close
            stage.setOnHiding(event -> {
                final String path = controller.getPath();
                final String encoding = controller.getEncoding();
                final String delimiter = controller.getDelimitter();

                //export csv
                if (path != null) {
                    CompletableFuture.runAsync(() -> {
                        onProgress();
                        try {
                            applicationService.exportCSV(outputList, new File(path), encoding, delimiter);
                        } catch (Exception e) {
                            LOGGER.log(Level.SEVERE, exceptionText, e);
                        }
                        onProgressStop();
                    });
                }
                stage.close();
            });
            stage.show();
        }));
    }

    public void onImportCSV(ActionEvent actionEvent) {
        final String fxml = "importCsv.fxml";
        final String title = "Import CSV";
        final String sheet = "SHEET";
        final String exceptionText = "Can not import csv";

        //create new window
        ControlUtils.createWindow(fxml, title).ifPresent((fxmlLoader -> {
            final Stage stage = ControlUtils.getStage(fxmlLoader);
            final ImportCsvController controller = fxmlLoader.getController();

            //if on window close
            stage.setOnHiding(event -> {
                final String path = controller.getPath();
                final String encoding = controller.getEncoding();
                final String delimiter = controller.getDelimitter();

                //import csv
                if (path != null) {
                    CompletableFuture.runAsync(() -> {
                        onProgress();
                        final String tableName = sheet + importTabPane.getTabs().size();
                        try {
                            final List<List<ValueModel>> list = applicationService.importCSV(new File(path), tableName, encoding, delimiter);
                            renderTab(list, tableName);
                        } catch (Exception e) {
                            LOGGER.log(Level.SEVERE, exceptionText, e);
                        }
                        onProgressStop();
                    });
                }
                stage.close();
            });
            stage.show();
        }));
    }

    public void onImportExcel(ActionEvent actionEvent) {
        final String tableName = "SHEET";
        final String fileDescription = "Excel File";
        final String fileExtension = "*.xlsx";
        final String title = "Open Excel";
        final String exceptionText = "Can not import excel";

        fileChooserOpen((Stage) main.getScene().getWindow(), fileDescription, fileExtension, title, file -> {
            createWindow("importExcel.fxml", "Import Excel").ifPresent(fxmlLoader -> {
                final Stage stage = ControlUtils.getStage(fxmlLoader);
                final ImportExcelController controller = fxmlLoader.getController();
                try {
                    controller.initPane(applicationService.getSheets(file));

                //if on window close
                stage.setOnHiding(event -> {
                    final int[] headerPositions = controller.getHeaderPosition();

                    //import excel
                    CompletableFuture.runAsync(() -> {
                        onProgress();
                        final int startNumber = importTabPane.getTabs().size();
                        try {
                            applicationService.importExcel(file, tableName, startNumber, headerPositions, this::renderTabSimple);
                        } catch (Exception e) {
                            try {
                                applicationService.remove(tableName);
                            } catch (SQLException e1) {
                                //do nothing
                            }
                            LOGGER.log(Level.SEVERE, exceptionText, e);
                        }
                        onProgressStop();
                    });
                    stage.close();
                });
                stage.show();

                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, exceptionText, e);
                }
            });
        });
    }

    private void renderTabSimple(List<ValueModel> list, String tableName) {
        final List<List<ValueModel>> fullList = new ArrayList<>();
        fullList.add(new ArrayList<>());
        fullList.get(0).addAll(list);
        renderTab(fullList, tableName);
    }

    private void renderTab(List<List<ValueModel>> list, String tableName) {
        Platform.runLater(() -> {
            Tab tab = importTabPane.getTabs().stream().filter(t -> t.getText().equals(tableName)).findFirst().orElse(null);
            if (tab == null) {
                tab = new Tab(tableName);
                final TableView<ObservableList<StringProperty>> csvTable = new TableView<>();
                TableUtils.installCopyPasteHandler(csvTable);
                tab.setContent(csvTable);
                tab.setOnClosed((e) -> {
                    try {
                        applicationService.remove(tableName);
                    } catch (SQLException e1) {
                        //do nothing
                    }
                });
                importTabPane.getTabs().add(tab);
            }

            final TableView<ObservableList<ValueModel>> tableview = (TableView<ObservableList<ValueModel>>) tab.getContent();
            TableUtils.updateTable(tableview, list, false);
        });
    }

    private void onProgress() {
        Platform.runLater(() -> {
            progressBar.setProgress(-1);
            sqlRunButton.setDisable(true);
            //fileMenu.setDisable(true);
            //themeMenu.setDisable(true);
        });
    }

    private void onProgressStop() {
        Platform.runLater(() ->
        {
            progressBar.setProgress(0);
            sqlRunButton.setDisable(false);
            //fileMenu.setDisable(false);
            //themeMenu.setDisable(false);
        });
    }

    public void run(ActionEvent actionEvent) {
        CompletableFuture.runAsync(() -> {
            onProgress();
            try {
                final String statement = sqlTextArea.getSelectedText().isEmpty() ? sqlTextArea.getText() : sqlTextArea.getSelectedText();
                outputList = applicationService.select(statement);
                Platform.runLater(() -> TableUtils.updateTable(outputTable, outputList, true));
            } catch (Exception e) {
                Platform.runLater(() -> TableUtils.clearTable(outputTable));
                LOGGER.info(e.getMessage());
            }
            onProgressStop();
        });
    }

    public void onExitApplication(ActionEvent actionEvent) {
        Platform.exit();
        System.exit(0);
    }

    public void onMetroDarkTheme(ActionEvent actionEvent) {
        Theme.applyJMetroDark(main.getScene());
    }

    public void onMetroLightTheme(ActionEvent actionEvent) {
        Theme.applyJMetroLight(main.getScene());
    }

    public void onResetTheme(ActionEvent actionEvent) {
        Theme.resetTheme(main.getScene());
    }

    public void onAbout(ActionEvent actionEvent) {
        final String title = "About";
        final String fxml = "about.fxml";

        ControlUtils.createWindow(fxml, title).ifPresent((fxmlLoader) -> getStage(fxmlLoader).show());
    }
}
