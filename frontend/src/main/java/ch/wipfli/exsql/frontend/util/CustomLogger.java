package ch.wipfli.exsql.frontend.util;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.*;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

public class CustomLogger {

    private static final String FORMAT = "[%1$tF %1$tT] [%2$-7s] %3$s %n";

    public static void init(TextArea textArea)
    {
        final Logger logger = Logger.getGlobal();
        logger.setLevel(Level.INFO);
        logger.setUseParentHandlers(false);

        //Formatter
        final SimpleFormatter formatter = new SimpleFormatter() {

        @Override
        public synchronized String format(LogRecord lr) {
            if(lr.getThrown() != null)
            {
                final StringWriter sw = new StringWriter();
                final PrintWriter pw = new PrintWriter(sw);
                lr.getThrown().printStackTrace(pw);
                final String stackTrace = sw.toString();

                return String.format(FORMAT,
                        new Date(lr.getMillis()),
                        lr.getLevel().getLocalizedName(),
                        stackTrace
                );
            }

            return String.format(FORMAT,
                    new Date(lr.getMillis()),
                    lr.getLevel().getLocalizedName(),
                    lr.getMessage()
            );
        }};

        //Handler
        final StreamHandler handler = new StreamHandler(new OutputStream() {

            @Override
            public void write(int b) {
                String s = String.valueOf((char)b);
                Platform.runLater(()-> textArea.appendText(s));
            }

        }, formatter){

            // flush on each publish:
            @Override
            public void publish(LogRecord record) {
                super.publish(record);
                flush();
            }
        };

        logger.addHandler(handler);
    }
}
