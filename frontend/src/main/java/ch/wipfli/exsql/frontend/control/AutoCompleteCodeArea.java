package ch.wipfli.exsql.frontend.control;

import javafx.geometry.Bounds;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;

import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;

import java.time.Duration;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Function;

public class AutoCompleteCodeArea extends CodeArea {
    /**
     * The existing autocomplete entries.
     */
    private final SortedSet<String> entries;
    /**
     * The popup used to select an entry.
     */
    private ContextMenu entriesPopup;

    private Function<String, List<String>> onChangeFunction;

    private Consumer<String> onApplyAutocompleteFunction;

    /**
     * Construct a new AutoCompleteTextField.
     */
    public AutoCompleteCodeArea() {
        super();
        entries = new TreeSet<>();
        entriesPopup = new ContextMenu();

        //Set linennumbers
        this.setParagraphGraphicFactory(LineNumberFactory.get(this));
        //Set listener
        this.multiPlainChanges()
                .successionEnds(Duration.ofMillis(500))
                .subscribe(l -> {
                    entriesPopup.hide();
                    entries.clear();
                    String text = getText();
                    if (getCaretPosition() > 0 && text.length() >= getCaretPosition()) {
                        //get text to caret
                        text = text.substring(0, getCaretPosition());
                        //get text till space
                        text = text.substring(text.lastIndexOf(' ') + 1).toUpperCase().trim();

                        //just text > 0
                        if (text.length() > 0) {
                            //add autocomplete entries with anonymous function.
                            entries.addAll(onChangeFunction.apply(text));
                            final LinkedList<String> searchResult = new LinkedList<>(entries);

                            //just if has autocomlete entriess
                            if (entries.size() > 0 && !entries.contains(text)) {
                                populatePopup(searchResult);
                                {
                                    //open popup on caret position
                                    caretBoundsProperty().getValue().ifPresent((bounds) -> entriesPopup.show(this, bounds.getMinX(), bounds.getMinY()));
                                }
                            }
                        }
                    }
                });

        //focusedProperty().addListener((observableValue, aBoolean, aBoolean2) -> entriesPopup.hide());
    }

    protected void onTextChangedAutocomplete(Function<String, List<String>> onChangeFunction) {
        this.onChangeFunction = onChangeFunction;
    }

    protected void onApplyAutocomplete(Consumer<String> onApplyAutocompleteFunction) {
        this.onApplyAutocompleteFunction = onApplyAutocompleteFunction;
    }


    /**
     * Populate the entry set with the given search results.  Display is limited to 10 entries, for performance.
     *
     * @param searchResult The set of matching strings.
     */
    private void populatePopup(List<String> searchResult) {
        final List<CustomMenuItem> menuItems = new LinkedList<>();
        // If you'd like more entries, modify this line.
        int maxEntries = 10000;
        int count = Math.min(searchResult.size(), maxEntries);
        for (int i = 0; i < count; i++) {
            final String result = searchResult.get(i);
            final Label entryLabel = new Label(result);
            final CustomMenuItem item = new CustomMenuItem(entryLabel, true);
            menuItems.add(item);

            //if choose text in popup
            item.setOnAction(actionEvent -> {
                //replace text
                final String text = getText();
                final String textAfterCater = text.substring(getCaretPosition(), getText().length());
                final String textTillCaret = text.substring(0, getCaretPosition());
                final String textTillToChange = text.substring(0, textTillCaret.lastIndexOf(' ') + 1);
                final String newText = textTillToChange + result + textAfterCater;

                //set text on codearea
                this.clear();
                this.replaceText(0, 0, newText);

                //set caret to new position
                this.moveTo((textTillToChange + result).length());
                //apply autocomplete listener
                onApplyAutocompleteFunction.accept(newText);

                //hide popup
                entriesPopup.hide();
            });
        }
        entriesPopup.getItems().clear();
        entriesPopup.getItems().addAll(menuItems);
    }
}
