package ch.wipfli.exsql.frontend.util;

import ch.wipfli.exsql.backend.ValueModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.*;

import java.util.List;

public class TableUtils {

    /**
     * Install the keyboard handler:
     * + CTRL + C = copy to clipboard
     *
     * @param table table
     */
    public static void installCopyPasteHandler(TableView<?> table) {

        table.getSelectionModel().setCellSelectionEnabled(true);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // install copy/paste keyboard handler
        table.setOnKeyPressed(new TableKeyEventHandler());

    }

    /**
     * Copy/Paste keyboard event handler.
     * The handler uses the keyEvent's source for the clipboard data. The source must be of type TableView.
     */
    public static class TableKeyEventHandler implements EventHandler<KeyEvent> {

        KeyCodeCombination copyKeyCodeCompination = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);

        public void handle(final KeyEvent keyEvent) {

            if (copyKeyCodeCompination.match(keyEvent)) {

                if (keyEvent.getSource() instanceof TableView) {

                    // copy to clipboard
                    copySelectionToClipboard((TableView<?>) keyEvent.getSource());

                    System.out.println("Selection copied to clipboard");

                    // event is handled, consume it
                    keyEvent.consume();

                }

            }

        }

    }

    /**
     * Get table selection and copy it to the clipboard.
     *
     * @param table table table
     */
    public static void copySelectionToClipboard(TableView<?> table) {

        StringBuilder clipboardString = new StringBuilder();

        ObservableList<TablePosition> positionList = table.getSelectionModel().getSelectedCells();

        int prevRow = -1;

        for (TablePosition position : positionList) {

            int row = position.getRow();
            int col = position.getColumn();

            Object cell = table.getColumns().get(col).getCellData(row);

            // null-check: provide empty string for nulls
            if (cell == null) {
                cell = "";
            }

            // determine whether we advance in a row (tab) or a column
            // (newline).
            if (prevRow == row) {

                clipboardString.append('\t');

            } else if (prevRow != -1) {

                clipboardString.append('\n');

            }

            // create string from cell
            String text = cell.toString();

            // add new item to clipboard
            clipboardString.append(text);

            // remember previous
            prevRow = row;
        }

        // create clipboard content
        final ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(clipboardString.toString());

        // set clipboard content
        Clipboard.getSystemClipboard().setContent(clipboardContent);
    }

    public static void clearTable(TableView table) {
            table.getItems().clear();
            table.getColumns().clear();
    }

    public static void updateTable(TableView<ObservableList<ValueModel>> table, List<List<ValueModel>> list, boolean refresh) {
        if(refresh) {
            table.getItems().clear();
            table.getColumns().clear();
        }
        TableUtils.installCopyPasteHandler(table);

        if (table.getColumns().size() == 0) {
            table.getColumns().addAll(getColumnsRow(list));
            table.getItems().addAll(getRows(1, list));
        }
        else
        {
            table.getItems().addAll(getRows(0, list));
        }
    }

    private static ObservableList<ObservableList<ValueModel>> getRows(int startPosition, List<List<ValueModel>> list) {
        final ObservableList<ObservableList<ValueModel>> result = FXCollections.observableArrayList();
        for (int p = startPosition; p < list.size(); p++) {
            final ObservableList<ValueModel> observableRow = FXCollections.observableArrayList();
            for (int i = 0; i < list.get(p).size(); i++) {
                observableRow.add(i, list.get(p).get(i));
            }
            result.add(observableRow);
        }
        return result;
    }

    private static List<TableColumn<ObservableList<ValueModel>, String>> getColumnsRow(List<List<ValueModel>> list) {
        final List<TableColumn<ObservableList<ValueModel>, String>> result = FXCollections.observableArrayList();
        for (int i = 0; i < list.get(0).size(); i++) {
            final String columnName = list.get(0).get(i).getValue() + " [" + list.get(0).get(i).getDatabaseTypeName() + "]";
            final TableColumn<ObservableList<ValueModel>, String> column = new TableColumn<>(columnName);
            final int position = i;
            column.setCellValueFactory(c ->
            {
                final ValueModel model = c.getValue().get(position);
                return new SimpleStringProperty(model.getStringValue());
            });
            result.add(column);
        }
        return result;
    }
}
