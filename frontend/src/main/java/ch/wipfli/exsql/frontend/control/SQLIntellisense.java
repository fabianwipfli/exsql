package ch.wipfli.exsql.frontend.control;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.*;

public class SQLIntellisense {

    private final LinkedHashSet<String> autoCompleteList = new LinkedHashSet<>();
    private final TabPane tabPane;

    public SQLIntellisense(TabPane tabPane, AutoCompleteCodeArea codeArea) {
        this.tabPane = tabPane;
        codeArea.onTextChangedAutocomplete(this::filter);
        init();
    }

    private void init() {
        final LinkedHashSet<String> autoCompleteSelectList = new LinkedHashSet<>();
        autoCompleteSelectList.add("SELECT");
        autoCompleteSelectList.add("TOP");
        autoCompleteSelectList.add("FROM");
        autoCompleteSelectList.add("DISTINCT");
        autoCompleteSelectList.add("LIMIT");

        final LinkedHashSet<String> autoCompleteWhereList = new LinkedHashSet<>();
        autoCompleteSelectList.add("WHERE");
        autoCompleteWhereList.add("AND");
        autoCompleteWhereList.add("OR");
        autoCompleteWhereList.add("IN");
        autoCompleteWhereList.add("NOT");
        autoCompleteWhereList.add("LIKE");
        autoCompleteWhereList.add("EXISTS");
        autoCompleteWhereList.add("GROUP BY");
        autoCompleteWhereList.add("HAVING");
        autoCompleteWhereList.add("ORDER BY");
        autoCompleteWhereList.add("DESC");
        autoCompleteWhereList.add("ASC");

        final LinkedHashSet<String> autoCompleteJoinList = new LinkedHashSet<>();
        autoCompleteJoinList.add("FULL OUTER JOIN");
        autoCompleteJoinList.add("JOIN");
        autoCompleteJoinList.add("INNER JOIN");
        autoCompleteJoinList.add("LEFT JOIN");
        autoCompleteJoinList.add("RIGHT JOIN");
        autoCompleteJoinList.add("OUTER JOIN");
        autoCompleteJoinList.add("UNION");
        autoCompleteJoinList.add("UNION ALL");

        autoCompleteList.addAll(autoCompleteSelectList);
        autoCompleteList.addAll(autoCompleteWhereList);
        autoCompleteList.addAll(autoCompleteJoinList);
    }

    private HashMap<String, LinkedHashSet<String>> getColumns(TabPane tabPane) {
        final HashMap<String, LinkedHashSet<String>> columnsList = new HashMap<>();

        //Add columns and tables names
        for (Tab tab : tabPane.getTabs()) {
            final LinkedHashSet<String> columns = new LinkedHashSet<>();
            final String tableName = tab.getText().toUpperCase();
            columnsList.put(tableName, columns);
            autoCompleteList.add(tableName);
            final TableView table = (TableView) tab.getContent();
            for (int i = 0; i < table.getColumns().size(); i++) {
                TableColumn column = (TableColumn) table.getColumns().get(i);
                columns.add(column.getText());
            }
        }
        return columnsList;
    }

    private List<String> filter(String searchText) {
        final LinkedHashSet<String> filterList = new LinkedHashSet<>();
        final HashMap<String, LinkedHashSet<String>> columnsMap = getColumns(tabPane);
        filterList.addAll(autoCompleteList);
        filterList.addAll(columnsMap.keySet());

        final List<String> list = new ArrayList<>();
        final String value = searchText.toUpperCase();

        for (String item : filterList) {
            if (item.startsWith(value)) {
                list.add(item);
            }
        }

        //Columns
        for (LinkedHashSet<String> items : columnsMap.values()) {
            for (String item : items) {
                final String columnName = item.substring(0, item.lastIndexOf("[") - 1);
                if (item.toUpperCase().startsWith(value)) {
                    list.add("\"" + columnName + "\"");
                }
            }
        }

        //Columns with .
        if (value.contains(".")) {
            for (LinkedHashSet<String> items : columnsMap.values()) {
                for (String item : items) {
                    final String columnName = item.substring(0, item.lastIndexOf("[") - 1);
                    final String prefixValue = value.substring(0, value.lastIndexOf(".")) + ".";
                    final String searchValue = value.substring(value.lastIndexOf("."));
                    if (("." + item.toUpperCase()).startsWith(searchValue)) {
                        list.add(prefixValue + "\"" + columnName + "\"");
                    }
                }
            }
        }
        return list;
    }
}
