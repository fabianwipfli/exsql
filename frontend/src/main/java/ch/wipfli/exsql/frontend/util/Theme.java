package ch.wipfli.exsql.frontend.util;

import javafx.scene.Scene;
import jfxtras.styles.jmetro8.JMetro;

public class Theme {

    public static void applyJMetroLight(Scene scene)
    {
        resetTheme(scene);
        JMetro metro = new JMetro(JMetro.Style.LIGHT);
        metro.applyTheme(scene);
    }

    public static void applyJMetroDark(Scene scene)
    {
        resetTheme(scene);
        JMetro metro = new JMetro(JMetro.Style.DARK);
        metro.applyTheme(scene);
    }

    public static void resetTheme(Scene scene)
    {
        scene.getStylesheets().remove(JMetro.class.getResource("JMetroDarkTheme.css").toExternalForm());

        scene.getStylesheets().remove(JMetro.class.getResource("JMetroLightTheme.css").toExternalForm());
    }
}
