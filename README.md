## SmartSQL

CSV oder Excel ist eines der bekanntesten Formate für das Speichern von Daten. Vor allem bei der Datenmigration werden CSV Dateien oft verwendet, um Daten von einer Datenbank zur Nächsten zu transformieren. Oft müssen die Daten zuerst noch analysiert und korrigiert werden, bevor sie in die Datenbank eingespielt werden können. 


Mit SmartSQL können gezielte Daten mit SQL-Statements untersucht und geändert werden. Um eine bequeme Art der Transformation von mehreren Dateien zu bewerkstelligen, ist es möglich mehreren CSV Dateien oder Excel Sheets mit einfachen SQL JOIN Statements zu verbinden. SmartSQL erlaubt es die Datenstrukturen spielerisch mit übersichtlichem SQL-Statements zu transformieren und mit einfach Klicks zu exportierten.  
 

[Java Application](https://bitbucket.org/fabianwipfli/exsql/downloads/app.zip).

[Online Application](https://smart-sql.herokuapp.com/).
