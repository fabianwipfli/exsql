import {Component, OnInit} from '@angular/core';
import {CsvService} from "../services/csv.service";
import {BlockUI, NgBlockUI} from "ng-block-ui";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @BlockUI()
  blockUI: NgBlockUI;

  exportActive: boolean;

  showDialog: boolean;

  isVisible: boolean;

  constructor(private csvService: CsvService) {
  }

  ngOnInit() {
    this.exportActive = true;

    this.csvService.onExcelExportFinished.subscribe(input => {
      this.blockUI.stop();
    });
    this.csvService.onCsvExportFinished.subscribe(input => {
      this.showDialog = false;
    });
  }

  exportCsv()
  {
    this.showDialog = true;
  }

  exportExcel()
  {
    this.blockUI.start('Download...');
    this.csvService.exportExcel(this.csvService.lastSelectStatement);
  }

}
