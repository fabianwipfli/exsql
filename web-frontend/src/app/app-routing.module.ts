import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {CsvUploadComponent} from './csv-upload/csv-upload.component';
import {CsvExportComponent} from "./csv-export/csv-export.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'export-csv', component: CsvExportComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
