import {AfterContentChecked, Component, OnInit} from '@angular/core';
import {CsvService} from '../services/csv.service';
import {Sheet} from '../services/sheet';

@Component({
  selector: 'app-output-sheets',
  templateUrl: './output-sheets.component.html',
  styleUrls: ['./output-sheets.component.css']
})
export class OutputSheetsComponent implements OnInit, AfterContentChecked {

  sheets: Sheet[] = [];
  active = 0;

  constructor(private csvService: CsvService) {
  }

  ngOnInit() {
    this.csvService.onSelectTableFinished.subscribe(input => {
      input.then(result => {
        this.sheets = result;
        this.active = 0;
      });
    });
  }

  ngAfterContentChecked(): void {
    this.resizeOutputTableHeight();
    this.resizeOutputCardBody();
  }

  resizeOutputTableHeight(): boolean {
    const table: HTMLElement = (document.querySelector('.output .ui-table-wrapper') as HTMLElement);
    if (table) {
      table.style.height = window.outerHeight / 2 - 170 + 'px';
    }
    return true;
  }

  resizeOutputCardBody(): boolean {
    const cardBody: HTMLElement = (document.querySelector('.output .card-body') as HTMLElement);
    if (cardBody) {
      cardBody.style.minHeight = window.outerHeight / 2 - 170 + 'px';
    }
    return true;
  }
}

