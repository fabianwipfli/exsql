import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputSheetsComponent } from './output-sheets.component';

describe('OutputSheetsComponent', () => {
  let component: OutputSheetsComponent;
  let fixture: ComponentFixture<OutputSheetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputSheetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputSheetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
