import {Component, Input, OnInit} from '@angular/core';
import {CsvService} from '../services/csv.service';
import {BlockUI, NgBlockUI} from "ng-block-ui";

@Component({
  selector: 'app-csv-export',
  templateUrl: './csv-export.component.html',
  styleUrls: ['./csv-export.component.css']
})

export class CsvExportComponent implements OnInit {

  @Input()
  showCSV: boolean;

  @Input()
  showExcel: boolean;

  @BlockUI()
  blockUI: NgBlockUI;

  encoding: string;
  delimiter: string;

  csvService: CsvService;

  constructor(csvService: CsvService) {
    this.csvService = csvService;
  }

  ngOnInit() {
    this.encoding = 'UTF-8';
    this.delimiter = ',';

    //Handler Csv
    this.csvService.onCsvExportFinished.subscribe(input => {
      this.blockUI.stop();
    });

    //Handler Excel
    this.csvService.onExcelExportFinished.subscribe(input => {
      this.blockUI.stop();
    });
  }

  onExportCsv()
  {
    this.blockUI.start('Download...');
    this.csvService.exportCsv(this.csvService.lastSelectStatement, this.encoding, this.delimiter);
  }

  onExportExcel()
  {
    this.blockUI.start('Download...');
    this.csvService.exportExcel(this.csvService.lastSelectStatement);
  }
}
