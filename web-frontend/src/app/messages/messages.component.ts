import {Component, OnInit} from '@angular/core';
import {Message} from 'primeng/components/common/api';
import {CsvService} from "../services/csv.service";

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  msgs: Message[] = [];

  constructor(private csvService: CsvService) {
  }

  ngOnInit() {

    //Message handling
    this.csvService.onExceptionOccurs.subscribe(input => {
      this.msgs = [];
      this.msgs.push({severity: 'warn', summary: 'SQL', detail: input.message});
    });

    this.csvService.onSelectTableFinished.subscribe(input => {
        this.msgs = [];
      }
    )
  }
}
