import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NavbarComponent} from './navbar/navbar.component';
import {AppRoutingModule} from './app-routing.module';
import {SqlInputComponent} from './sql-input/sql-input.component';
import {SheetsComponent} from './sheets/sheets.component';
import {OutputSheetsComponent} from './output-sheets/output-sheets.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {CsvUploadComponent} from './csv-upload/csv-upload.component';
import {HomeComponent} from './home/home.component';

import {FileUploadModule} from 'primeng/fileupload';
import {TableModule} from 'primeng/table';
import {OutputConsoleComponent} from './output-console/output-console.component';
import {BlockUIModule} from 'ng-block-ui';

import {ChartModule, DialogModule, MessageModule, MessagesModule} from "primeng/primeng";
import {MessagesComponent} from './messages/messages.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CsvExportComponent} from './csv-export/csv-export.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SqlInputComponent,
    SheetsComponent,
    OutputSheetsComponent,
    OutputConsoleComponent,
    CsvUploadComponent,
    HomeComponent,
    MessagesComponent,
    CsvExportComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    TableModule,
    FileUploadModule,
    MessagesModule,
    MessageModule,
    ChartModule,
    DialogModule,
    BrowserAnimationsModule,
    BlockUIModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
