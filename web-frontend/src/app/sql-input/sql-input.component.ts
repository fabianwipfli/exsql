import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {CsvService} from '../services/csv.service';
import {Sheet} from '../services/sheet';

import * as ace from 'ace-builds';
import 'ace-builds/src-noconflict/mode-sql';
import 'ace-builds/src-noconflict/theme-github';
import 'ace-builds/src-noconflict/ext-language_tools';
import 'ace-builds/src-noconflict/ext-beautify';

const INIT_CONTENT = '';
const THEME = 'ace/theme/github';
const LANG = 'ace/mode/sql';

@Component({
  selector: 'app-sql-input',
  templateUrl: './sql-input.component.html',
  styleUrls: ['./sql-input.component.css']
})

export class SqlInputComponent implements AfterViewInit, OnInit {

  private autocompleteList: string[];
  private codeEditor: ace.Ace.Editor;
  private editorBeautify;

  @ViewChild('codeEditor', {static: true})
  private codeEditorElmRef: ElementRef;

  @Input()
  content: string;

  constructor(private csvService: CsvService) { }

  ngAfterViewInit() {
    this.editorBeautify = ace.require('ace/ext/beautify');
    ace.require('ace/ext/language_tools');

    const element = this.codeEditorElmRef.nativeElement;
    const editorOptions = this.getEditorOptions();
    this.codeEditor = this.createCodeEditor(element, editorOptions);
    this.setContent(this.content || INIT_CONTENT);
  }


  private createCodeEditor(element: Element, options: any): ace.Ace.Editor {
    const editor = ace.edit(element, options);
    editor.setTheme(THEME);
    editor.getSession().setMode(LANG);
    editor.setShowFoldWidgets(true);
    editor.completers.push(this.getCompleter());
    return editor;
  }

  private getCompleter() {
    const wordList = this.autocompleteList;
    const staticWordCompleter = {
      getCompletions: (editor, session, pos, prefix, callback) => {
        callback(null, wordList.map(word => {
          return {
            caption: word,
            value: word,
            meta: 'table'
          };
        }));
      }
    };
    return staticWordCompleter;
  }

  // missing propery on EditorOptions 'enableBasicAutocompletion' so this is a wolkaround still using ts
  private getEditorOptions(): Partial<ace.Ace.EditorOptions> & { enableBasicAutocompletion?: boolean; } {
    const basicEditorOptions: Partial<ace.Ace.EditorOptions> = {
      highlightActiveLine: true,
      minLines: 14,
      maxLines: Infinity,
    };
    const extraEditorOptions = {
      enableBasicAutocompletion: true,
      enableLiveAutocompletion: true
    };
    return Object.assign(basicEditorOptions, extraEditorOptions);
  }

  /**
   * @returns - the current editor's content.
   */
  public getContent() {
    if (this.codeEditor) {
      if(this.codeEditor.getSelectedText().length==0) {
        return this.codeEditor.getValue();
      }
      else
      {
        return this.codeEditor.getSelectedText();
      }
    }
  }

  /**
   * @param content - set as the editor's content.
   */
  public setContent(content: string): void {
    if (this.codeEditor) {
      this.codeEditor.setValue(content);
    }
  }

  /**
   * @description
   *  beautify the editor content, rely on Ace Beautify extension.
   */
  public beautifyContent() {
    if (this.codeEditor && this.editorBeautify) {
      const session = this.codeEditor.getSession();
      this.editorBeautify.beautify(session);
    }
  }

  /**
   * @event OnContentChange - a proxy event to Ace 'change' event - adding additional data.
   * @param callback - recive the corrent content and 'change' event's original parameter.
   */
  public OnContentChange(callback: (content: string, delta: ace.Ace.Delta) => void): void {
    this.codeEditor.on('change', (delta) => {
      const content = this.codeEditor.getValue();
      callback(content, delta);
    });
  }

  ngOnInit(): void {
    this.autocompleteList = [];

    // Load autocomplete list
    this.csvService.onLoadTableFinished.subscribe((input: Promise<Sheet[]>) => {
      input.then((result: Sheet[]) => {
        // this.autocompleteList;
        for (const sheet of result) {
          this.autocompleteList.push(sheet.name);
          for (const column of sheet.columns) {
            this.autocompleteList.push(this.hasStringSpace(column.stringValue) ? '"' + column.stringValue + '"' : column.stringValue);
          }
        }
      });
    });

    // Load autocomplete list remove sheet
    this.csvService.onDeleteTableFinished.subscribe((input: Promise<Sheet[]>) => {
      input.then((result: Sheet[]) => {
        // this.autocompleteList;
        for (const sheet of result) {
          this.removeFromAutocompleteList(sheet.name);
          for (const column of sheet.columns) {
            this.removeFromAutocompleteList(this.hasStringSpace(column.stringValue) ? '"' + column.stringValue + '"' : column.stringValue);
          }
        }
      });
    });
  }

  hasStringSpace(content: string): boolean {
    if (/\s/.test(content)) {
      return true;
    }
    return false;
  }

  removeFromAutocompleteList(item: string) {
    for (let i = this.autocompleteList.length - 1; i >= 0; i--) {
      if (this.autocompleteList[i] === item) {
        this.autocompleteList.splice(i, 1);
        break;
      }
    }
  }

  onSelect() {
    this.csvService.select(this.getContent());
  }
}

