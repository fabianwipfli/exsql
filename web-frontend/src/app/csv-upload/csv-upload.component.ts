import {Component, OnInit} from '@angular/core';
import {CsvService} from '../services/csv.service';

@Component({
  selector: 'app-csv-upload',
  templateUrl: './csv-upload.component.html',
  styleUrls: ['./csv-upload.component.css']
})
export class CsvUploadComponent implements OnInit {

  uploadedFile: any;
  encoding: string;
  delimiter: string;
  row: string;
  showCSV: boolean;
  showExcel: boolean;
  csvService: CsvService;

  constructor(csvService: CsvService) {
    this.csvService = csvService;
  }

  ngOnInit() {
    this.encoding = 'UTF-8';
    this.delimiter = ',';
    this.row = '1';
    this.showCSV = false;
    this.showExcel = false;
    this.uploadedFile = null;
  }

  onUpload() {
    if (this.uploadedFile !== '') {
      if (this.showCSV) {
        this.csvService.importCsv(this.uploadedFile, this.encoding, this.delimiter);
      } else {
        this.csvService.importExcel(this.uploadedFile, this.row);
      }
    }
  }

  onRemove(event) {
    this.uploadedFile = null;
    this.showCSV = false;
    this.showExcel = false;
  }

  onFile(event) {
    for (const file of event.files) {
      this.uploadedFile = file;
      const ext = file.name.split('.').pop();
      if (ext.toUpperCase() === 'CSV') {
        this.showCSV = true;
        this.showExcel = false;
      } else {
        this.showCSV = false;
        this.showExcel = true;
      }
    }
  }

}
