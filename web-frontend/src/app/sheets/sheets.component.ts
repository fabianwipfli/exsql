import {AfterContentChecked, Component, OnChanges, OnInit} from '@angular/core';
import {CsvService} from '../services/csv.service';
import {Sheet} from '../services/sheet';
import {BlockUI, NgBlockUI} from 'ng-block-ui';

@Component({
  selector: 'app-sheets',
  templateUrl: './sheets.component.html',
  styleUrls: ['./sheets.component.css']
})
export class SheetsComponent implements OnInit, AfterContentChecked {

  @BlockUI()
  blockUI: NgBlockUI;

  sheets: Sheet[] = [];
  active = -1;

  constructor(private csvService: CsvService) {
  }

  ngOnInit() {
    this.csvService.onLoadTableFinished.subscribe(input => {
      input.then(result => {
        if (this.sheets.length > 0) {
          Array.prototype.push.apply(this.sheets, result);
        } else {
          this.sheets = result;
        }
        this.active = 0;
        this.blockUI.stop();
      });
    });
    this.csvService.onLoadTableStart.subscribe(input => {
      this.blockUI.start('Load sheets...');
    });
    this.csvService.getSheets();
  }

  onClose(index: number) {
    this.csvService.removeSheet(this.sheets[index].name);
    this.sheets.splice(index, 1);
  }

  ngAfterContentChecked(): void {
    this.resizeSheetTableHeight();
    this.resizeSheetCardBody();
  }

  resizeSheetTableHeight() {
    const table: HTMLElement = (document.querySelector('.sheet .ui-table-wrapper') as HTMLElement);
    if (table) {
      table.style.height = window.outerHeight / 2 - 220 + 'px';
    }
    return true;
  }

  resizeSheetCardBody() {
    const cardBody: HTMLElement = (document.querySelector('.sheet .card-body') as HTMLElement);
    if (cardBody) {
      cardBody.style.minHeight = window.outerHeight / 2 - 220 + 'px';
    }
    return true;
  }
}
