interface Column {
  value: string;
  databaseType: number;
  classType: string;
  databaseTypeName: string;
  stringValue: string;
}
