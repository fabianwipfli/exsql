import {EventEmitter, Injectable, Output} from '@angular/core';
import {Sheet} from './sheet';

@Injectable({
  providedIn: 'root'
})
export class CsvService {

  port: string;
  endpoint = '/service/';
  endpointImportCsv = this.endpoint + 'uploadCsv';
  endpointImportExcel = this.endpoint + 'uploadExcel';
  endpointSelect = this.endpoint + 'select';
  endpointSheets = this.endpoint + 'sheets';
  endpointExportCsv = this.endpoint + 'exportCsv';
  endpointExportExcel = this.endpoint + 'exportExcel';

  @Output()
  onLoadTableStart: EventEmitter<any> = new EventEmitter();
  @Output()
  onLoadTableFinished: EventEmitter<Promise<Sheet[]>> = new EventEmitter();
  @Output()
  onSelectTableFinished: EventEmitter<Promise<Sheet[]>> = new EventEmitter();
  @Output()
  onDeleteTableFinished: EventEmitter<Promise<Sheet[]>> = new EventEmitter();
  @Output()
  onExceptionOccurs: EventEmitter<string> = new EventEmitter();
  @Output()
  onExcelExportFinished: EventEmitter<string> = new EventEmitter();
  @Output()
  onCsvExportFinished: EventEmitter<string> = new EventEmitter();

  lastSelectStatement: string;

  constructor() {
  }

  async importCsv(file: any, encoding: string, delimiter: string) {
    try {
      const form = new FormData();
      form.append('file', file, file.name);
      form.append('encoding', encoding);
      form.append('delimiter', delimiter);
      form.append('sheetName', 'SHEET');
      form.append('session_key', this.getSessionKey());

      this.onLoadTableStart.emit(null);

      const result = await fetch(this.endpointImportCsv, {
        method: 'POST',
        body: form,
        mode: 'cors',
        /*headers: new Headers({
          'Content-Type': 'multipart/form-data'
        })*/
      });
      this.onLoadTableFinished.emit(result.json());
    } catch (error) {
      this.onLoadTableFinished.emit(error);
      console.log(error);
    }
  }

  async importExcel(file: any, headerPosition: string) {
    try {
      const form = new FormData();
      form.append('file', file, file.name);
      form.append('name', 'SHEET');
      form.append('headerPosition', headerPosition);
      form.append('session_key', this.getSessionKey());

      this.onLoadTableStart.emit(null);

      const result = await fetch(this.endpointImportExcel, {
        method: 'POST',
        body: form,
        mode: 'cors',
        /*headers: new Headers({
          'Content-Type': 'multipart/form-data'
        })*/
      });
      this.onLoadTableFinished.emit(result.json());
    } catch (error) {
      this.onLoadTableFinished.emit(error);
      console.log(error);
    }
  }

  async removeSheet(sheetName: string) {
    try {
      const params = {session_key: this.getSessionKey()};
      const urlParams = new URLSearchParams(Object.entries(params));
      const result = await fetch(this.endpoint + sheetName + '/?' + urlParams.toString(), {
        method: 'DELETE',
        mode: 'cors',
      });
      this.onDeleteTableFinished.emit(result.json());
    } catch (error) {
      this.onDeleteTableFinished.emit(error);
      console.log(error);
    }
  }

  async select(statement: string) {
    try {
      const params = {statement: statement, session_key: this.getSessionKey()};
      const urlParams = new URLSearchParams(Object.entries(params));
      const result = await fetch(this.endpointSelect + '/?' + urlParams.toString(), {
        method: 'GET',
        mode: 'cors'
      });
      if (result.ok) {
        this.onSelectTableFinished.emit(result.json());
        this.lastSelectStatement = statement;
      }
      else {
        const exception = await result.json();
        this.onExceptionOccurs.emit(exception);
      }
    } catch (error) {
      this.onExceptionOccurs.emit(error.message);
      console.log(error);
    }
  }

  async getSheets() {
    try {
      this.onLoadTableStart.emit();
      const params = {session_key: this.getSessionKey()};
      const urlParams = new URLSearchParams(Object.entries(params));
      const result = await fetch(this.endpointSheets + '/?' + urlParams.toString(), {
        method: 'GET',
        mode: 'cors'
      });
      this.onLoadTableFinished.emit(result.json());
    } catch (error) {
      this.onLoadTableFinished.emit(error);
      console.log(error);
    }
  }

  async exportCsv(statement: string, encoding: string, delimitter: string) {
    try {
      const params = {
        session_key: this.getSessionKey(),
        statement: statement,
        encoding: encoding,
        delimitter: delimitter
      };
      const urlParams = new URLSearchParams(Object.entries(params));
      const result = await fetch(this.endpointExportCsv + '/?' + urlParams.toString(), {
        method: 'GET',
        mode: 'cors'
      });
      const resultBlob = await result.blob();
      console.log(resultBlob);
      const blob = new Blob([resultBlob], {type: 'text/csv'});
      var downloadURL = window.URL.createObjectURL(blob);
      var link = document.createElement('a');
      document.body.appendChild(link);
      link.href = downloadURL;
      link.download = "result.csv";
      link.click();
      this.onCsvExportFinished.emit(link.download);
    } catch (error) {
      error.json().then(input => {
        console.log(input)
      });
    }
  }

  async exportExcel(statement: string) {
    try {
      const params = {
        session_key: this.getSessionKey(),
        statement: statement,
      };
      const urlParams = new URLSearchParams(Object.entries(params));
      const result = await fetch(this.endpointExportExcel + '/?' + urlParams.toString(), {
        method: 'GET',
        mode: 'cors'
      });
      const resultBlob = await result.blob();
      console.log(resultBlob);
      const blob = new Blob([resultBlob], {type: 'application/vnd.ms-excel'});
      var downloadURL = window.URL.createObjectURL(blob);
      var link = document.createElement('a');
      document.body.appendChild(link);
      link.href = downloadURL;
      link.download = "result.xlsx";
      link.click();
      this.onExcelExportFinished.emit(link.download);
    } catch (error) {
      error.json().then(input => {
        console.log(input)
      });
    }
  }

  getLastSelectStatement(): string {
    return this.lastSelectStatement;
  }

  getSessionKey(): string {
    let key = localStorage.getItem('session_key');
    if (key == null) {
      const newKey = Math.random().toString(20) + '-' + Math.random().toString(20);
      localStorage.setItem('session_key', newKey);
      key = localStorage.getItem('session_key');
    }
    return key;
  }
}
