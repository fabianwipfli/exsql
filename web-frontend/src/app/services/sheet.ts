export interface Sheet {
  name: string;
  columns: Column[];
  content: string[][];
}
