package ch.wipfli.exsql.backend;

import java.util.ArrayList;
import java.util.List;

public class SheetModel {
    private String name;
    private List<ValueModel> columns;
    private List<List<String>> content;

    public SheetModel()
    {
        content = new ArrayList<>();
        columns = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<ValueModel> getColumns() {
        return columns;
    }

    public List<List<String>> getContent() {
        return content;
    }

    public void setName(String name) {
        this.name = name;
    }
}
