package ch.wipfli.exsql.backend;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ApplicationServiceImpl implements ApplicationService {

    private final static Logger LOGGER = Logger.getGlobal();


    /*public static void main(String[] args) throws IOException, SQLException {
        String file = "C:\\Users\\HSR\\IdeaProjects\\exsql\\backend\\kunden_v1.2.xlsx";
        ApplicationService db = new ApplicationServiceImpl();
        db.importExcel(new File(file), "tab", 1, new int[0], (x, y) -> {
        });
        List<List<ValueModel>> importExcel = db.select("select * from tab1");
        db.exportExcel(importExcel, new File("C:\\Users\\HSR\\Downloads\\SampleDataExport.xlsx"));
        db.exportCSV(importExcel, new File("C:\\Users\\HSR\\Downloads\\SampleDataExport.csv"), "UTF-8", ",");
    }*/

    @Override
    public List<SheetModel> test() throws IOException, SQLException {
        final File file = new File("C:\\Users\\HSR\\Downloads\\_sample.csv");
        return importCSVExt(file, "sheet1", "UTF-8", ",","test");
    }

    @Override
    public List<List<ValueModel>> select(String statement) throws SQLException {
        return Database.selectStatement(statement, "db");
    }

    @Override
    public List<List<ValueModel>> select(String statement, String sessionKey) throws SQLException {
        final List<List<ValueModel>>  list = new ArrayList<>();
        final String[] statements = statement.split(";");
        for (int i = 0; i < statements.length; i++) {
            while (statements[i].startsWith("\n")) {
                statements[i] = statements[i].replaceFirst("\n", "");
            }
            if(!statements[i].isEmpty()) {
                list.addAll(Database.selectStatement(statement, sessionKey));
            }
        }
        return list;
    }

    @Override
    public List<List<List<ValueModel>>> selectMultiple(String statement, String sessionKey) throws SQLException {
        final List<List<List<ValueModel>>>  list = new ArrayList<>();
        final String[] statements = statement.split(";");
        for (int i = 0; i < statements.length; i++) {
            while (statements[i].startsWith("\n")) {
                statements[i] = statements[i].replaceFirst("\n", "");
            }
            if(!statements[i].isEmpty()) {
                list.add(Database.selectStatement(statement, sessionKey));
            }
        }
        return list;
    }

    @Override
    public List<SheetModel> selectExt(String statement, String sessionKey) throws SQLException {
        List<SheetModel> list = new ArrayList<>();
        final String[] statements = statement.split(";");
        for (int i = 0; i < statements.length; i++) {
            while (statements[i].startsWith("\n")) {
                statements[i] = statements[i].replaceFirst("\n", "");
            }
            if(!statements[i].isEmpty()) {
                list.add(Database.selectStatement(statements[i], "RESULT" + i, sessionKey));
            }
        }
        return list;
    }

    @Override
    public List<SheetModel>  remove(String tableName) throws SQLException {
        return remove(tableName, "db");
    }

    @Override
    public List<SheetModel>  remove(String tableName, String sessionKey) throws SQLException {
        List<SheetModel> sheets = selectExt("select top 1 * from " + tableName, sessionKey);
        Database.executeStatement(MessageFormat.format("DROP TABLE IF EXISTS {0} ;", tableName), sessionKey);
        LOGGER.info(MessageFormat.format("Sheet {0} removed.", tableName));
        return sheets;
    }

    @Override
    public void importExcel(File file, String name, int nameStartNumber, int[] headerPosition, BiConsumer<List<ValueModel>, String> callback) throws IOException, SQLException {
        final String sessionName = "db";
        final ExcelReader excel = new ExcelReader();
        final List<List<List<ValueModel>>> excelResultList = excel.importExcel(file, headerPosition);

        for (List<List<ValueModel>> excelResult : excelResultList) {
            final String tableName = name + nameStartNumber;
            final String statement = createImportStatement(excelResult, name + nameStartNumber, callback);
            LOGGER.info(MessageFormat.format("Import file {0} as {1}", file, tableName));
            Database.executeStatement(statement, sessionName);

            LOGGER.info(MessageFormat.format("Finish import file {0} sheet{1} as {2}", file, nameStartNumber, tableName));

            nameStartNumber++;
        }
    }

    @Override
    public List<SheetModel> importExcel(File file, String tableName, int[] headerPosition, String sessionKey) throws IOException, SQLException {
        final ExcelReader excel = new ExcelReader();
        int count = Integer.parseInt(Database.selectStatement("show tables", "Result", sessionKey).getContent().stream()
                .map(f->f.get(0))
                .map(f->f.replace("SHEET", ""))
                .sorted()
                .findFirst()
                .orElse("-1"));
        count++;

        final List<SheetModel> excelResultList = excel.importExcel(file, tableName, count, headerPosition, sessionKey);

        for (SheetModel excelResult : excelResultList) {
            final String statement = createImportStatement(excelResult, tableName + count);
            LOGGER.info(MessageFormat.format("Import file {0} as {1}", file, tableName + count));
            Database.executeStatement(statement, sessionKey);

            LOGGER.info(MessageFormat.format("Finish import file {0} sheet{1} as {2}", file, count, tableName+ count));

            count++;
        }

        return excelResultList;
    }

    @Override
    public void importCSV(File file, String tableName, String encoding, String delimiter, BiConsumer<List<ValueModel>, String> callback) throws IOException {
        final String databaseName = "db";
        final ExcelReader excel = new ExcelReader();
        final List<List<ValueModel>> excelResult = excel.importCSV(file, encoding, delimiter);

        LOGGER.info(MessageFormat.format("Import file {0} as {1}", file, tableName));
        final String statement = createImportStatement(excelResult, tableName, callback);
        Database.executeStatement(statement, databaseName);

        LOGGER.info(MessageFormat.format("Finish import file {0} as {1}", file, tableName));
    }

    @Override
    public List<List<ValueModel>> importCSV(File file, String tableName, String encoding, String delimiter) throws SQLException {
        final String databaseName = "db";
        final String selectStatement = MessageFormat.format("select * from {0}", tableName);

        String statement = MessageFormat.format("DROP TABLE IF EXISTS {0}; CREATE TABLE {0} AS SELECT * FROM CSVREAD(''{1}'', null, ''charset={2} fieldSeparator={3}'');", tableName, file.getAbsolutePath(), encoding, delimiter);

        LOGGER.fine(statement);
        Database.executeStatement(statement, databaseName);

        LOGGER.fine(selectStatement);
        return new ArrayList<>(Database.selectStatement(selectStatement, databaseName));
    }

    @Override
    public List<SheetModel> importCSVExt(File file, String tableName, String encoding, String delimiter, String sessionKey) throws SQLException {
        final List<SheetModel> result = new ArrayList<>();
        int count = Integer.parseInt(Database.selectStatement("show tables", "Result", sessionKey).getContent().stream()
                .map(f->f.get(0))
                .map(f->f.replace("SHEET", ""))
                .sorted()
                .findFirst()
                .orElse("-1"));
        count++;
        tableName = tableName + count;

        final String selectStatement = MessageFormat.format("select * from {0}", tableName);

        String statement = MessageFormat.format("DROP TABLE IF EXISTS {0}; CREATE TABLE {0} AS SELECT * FROM CSVREAD(''{1}'', null, ''charset={2} fieldSeparator={3}'');", tableName, file.getAbsolutePath(), encoding, delimiter);

        LOGGER.fine(statement);
        Database.executeStatement(statement, sessionKey);

        LOGGER.fine(selectStatement);
        result.add(Database.selectStatement(selectStatement, tableName, sessionKey));

        return result;
    }

    @Override
    public void exportExcel(List<List<ValueModel>> list, File file) throws IOException {
        if (list == null || list.size() == 0) {
            throw new IOException("No data found");
        }
        final List<List<List<ValueModel>> > sheets = new ArrayList<>();
        sheets.add(list);
        final ExcelExport export = new ExcelExport();
        export.exportExcel(sheets, file);
        LOGGER.info(MessageFormat.format("File {0} exported", file));
    }

    @Override
    public void exportExcelMultiple(List<List<List<ValueModel>>> list, File file) throws IOException {
        if (list == null || list.size() == 0) {
            throw new IOException("No data found");
        }
        final ExcelExport export = new ExcelExport();
        export.exportExcel(list, file);
        LOGGER.info(MessageFormat.format("File {0} exported", file));
    }

    @Override
    public void exportCSV(List<List<ValueModel>> list, File file, String encoding, String delimiter) throws IOException {
        if (list == null || list.size() == 0) {
            throw new IOException("No data found");
        }

        final ExcelExport export = new ExcelExport();
        export.exportCSV(list, file, encoding, delimiter);
        LOGGER.info(MessageFormat.format("File {0} exported", file));
    }

    private String createImportStatement(SheetModel list, String tableName) {
        String createStatement = "DROP TABLE IF EXISTS " + tableName + "; CREATE TABLE " + tableName + " (";
        String insertStatementPrefix = "INSERT INTO " + tableName + " (";

        //Create table statement
        final List<ValueModel> firstRow = list.getColumns();
        for (int i = 0; i < firstRow.size(); i++) {
            final Object value = firstRow.get(i).getStringValue();
            final String datatype = firstRow.get(i).getDatabaseTypeName();

            createStatement += " \"" + value + "\" " + datatype;
            if (i < firstRow.size() - 1) {
                createStatement += ",";
            } else {
                createStatement += ");";
            }
        }
        LOGGER.fine(createStatement);

        //Insert into table statement
        for (ValueModel cell : firstRow) {
            insertStatementPrefix += " \"" + cell.getStringValue() + "\"";
            if (list.getColumns().indexOf(cell) < list.getColumns().size() - 1) {
                insertStatementPrefix += ",";
            } else {
                insertStatementPrefix += ")VALUES(";
            }
        }

        String concatInsertStatement = "";
        for (int i = 0; i < list.getContent().size(); i++) {
            String insertStatement = insertStatementPrefix;
            for (int p = 0; p < list.getContent().get(i).size(); p++) {
                final String value = list.getContent().get(i).get(p);
                insertStatement += " " + value;

                if (p < list.getContent().get(i).size() - 1) {
                    insertStatement += ",";
                } else {
                    insertStatement += ");";
                }
            }
            concatInsertStatement += insertStatement;
        }

        return createStatement + concatInsertStatement;
    }

    private String createImportStatement(List<List<ValueModel>> list, String tableName, BiConsumer<List<ValueModel>, String> function) {
        String createStatement = "DROP TABLE IF EXISTS " + tableName + "; CREATE TABLE " + tableName + " (";
        String insertStatementPrefix = "INSERT INTO " + tableName + " (";

        //Create table statement
        final List<ValueModel> firstRow = list.get(0);
        final List<ValueModel> secondRow = list.get(1);
        for (int i = 0; i < firstRow.size(); i++) {
            final Object value = firstRow.get(i).getStringValue();

            //Type from next row.
            final String datatype = secondRow.get(i).getDatabaseTypeName();

            createStatement += " \"" + value + "\" " + datatype;
            if (i < list.get(0).size() - 1) {
                createStatement += ",";
            } else {
                createStatement += ");";
            }
        }
        LOGGER.fine(createStatement);

        //Insert into table statement
        for (ValueModel cell : firstRow) {
            insertStatementPrefix += " \"" + cell.getStringValue() + "\"";
            if (list.get(0).indexOf(cell) < list.get(0).size() - 1) {
                insertStatementPrefix += ",";
            } else {
                insertStatementPrefix += ")VALUES(";
            }
        }

        function.accept(list.get(0), tableName);
        list.remove(0);

        String concatInsertStatement = "";
        for (int i = 0; i < list.size(); i++) {
            final int rowCount = i;
            if (function != null) {
                CompletableFuture.runAsync(() -> function.accept(list.get(rowCount), tableName));
            }
            String insertStatement = insertStatementPrefix;
            for (int p = 0; p < list.get(i).size(); p++) {
                final String value = DatatypeMapper.getDatabaseValue(list.get(i).get(p));
                insertStatement += " " + value;

                if (p < list.get(i).size() - 1) {
                    insertStatement += ",";
                } else {
                    insertStatement += ");";
                }
            }
            concatInsertStatement += insertStatement;
        }

        return createStatement + concatInsertStatement;
    }

    @Override
    public String[] getSheets(File file) throws IOException {
        ExcelReader reader = new ExcelReader();
        return reader.getSheets(file);
    }

    @Override
    public List<SheetModel> getSheets(String sessionKey) throws SQLException {
        final List<SheetModel> tables = new ArrayList<>();
        for (List<String> row : Database.selectStatement("show tables", "Result", sessionKey).getContent()) {
            String tableName = row.get(0);
            tables.add(Database.selectStatement("select * from " + tableName, tableName, sessionKey));
        }

        return tables;
    }
}
