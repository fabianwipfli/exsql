package ch.wipfli.exsql.backend;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ExcelExport {

    /**
     * New line character for CSV file
     */
    private static final String NEW_LINE_CHARACTER = "\r\n";

    /**
     * Name of sheet
     */
    private static final String SHEET_NAME = "Sheet";


    private Workbook createWorkbook(List<List<List<ValueModel>>> list) {
        // Create a Workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        final CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(workbook.getCreationHelper().createDataFormat().getFormat(DatatypeMapper.DATE_FORMAT));

        for(int r = 0;r< list.size();r++) {
            List<List<ValueModel>> sheetRow = list.get(r);

            // Create a Sheet
            Sheet sheet = workbook.createSheet("RESULT" + r);

            // Create a Row
            Row headerRow = sheet.createRow(0);
            for (int i = 0; i < sheetRow.get(0).size(); i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(sheetRow.get(0).get(i).getStringValue());
            }

            for (int i = 1; i < sheetRow.size(); i++) {
                Row row = sheet.createRow(i);
                for (int p = 0; p < sheetRow.get(i).size(); p++) {
                    Cell cell = row.createCell(p);
                    Class classType = sheetRow.get(i).get(p).getClassType();
                    Object value = sheetRow.get(i).get(p).getValue();
                    if (classType == Date.class) {
                        cell.setCellValue((Date) value);
                    } else if (classType == Integer.class) {
                        cell.setCellValue((Integer) value);
                    } else if (classType == Double.class) {
                        cell.setCellValue((Double) value);
                    } else if (classType == Timestamp.class) {
                        cell.setCellValue((Timestamp) value);
                        cell.setCellStyle(dateCellStyle);
                    } else if (classType == BigDecimal.class) {
                        cell.setCellValue(((BigDecimal) value).doubleValue());
                    } else {
                        cell.setCellValue((String) value);
                    }
                }
                sheet.autoSizeColumn(i);
            }
        }

        return workbook;
    }

    protected void exportExcel(List<List<List<ValueModel>>> list, File file) throws IOException {
        try (Workbook workbook = createWorkbook(list)) {
            // Write the output to a file
            try (FileOutputStream fileOut = new FileOutputStream(file)) {
                workbook.write(fileOut);
            }
        }
    }

    protected void exportCSV(List<List<ValueModel>> list, File file, String encoding, String delimiter) throws IOException {
        String csvData = "";
        for (List<ValueModel> rows : list) {
            for (ValueModel item : rows) {
                csvData += item.getStringValue();
                csvData += delimiter;
            }
            csvData += NEW_LINE_CHARACTER;
        }

        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), encoding)) {
            writer.write(csvData);
        }
    }
}
