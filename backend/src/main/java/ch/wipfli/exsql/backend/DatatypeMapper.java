package ch.wipfli.exsql.backend;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.sql.Types.*;

public class DatatypeMapper {

    public final static String DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";

    public static Object getValue(Cell cell) {
        if (cell == null) {
            return null;
        }
        switch (cell.getCellType()) {
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue();
                }
                return cell.getNumericCellValue();
            case BOOLEAN:
                return cell.getBooleanCellValue();
            case FORMULA:
                switch (cell.getCachedFormulaResultType()) {
                    case NUMERIC:
                        return cell.getNumericCellValue();
                    case STRING:
                        return cell.getRichStringCellValue();
                }
            default:
                return cell.getStringCellValue();
        }
    }

    public static int getDatabaseType(Cell cell) {
        if (cell == null) {
            return VARCHAR;
        }
        switch (cell.getCellType()) {
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return TIMESTAMP;
                }
                return NUMERIC;
            case BOOLEAN:
                return INTEGER;
            default:
                return VARCHAR;
        }
    }

    public static String getDatabaseValue(ValueModel value) {
        if (value.getValue() instanceof Date) {
            String result = new SimpleDateFormat(DATE_FORMAT).format(value.getValue());
            result = "'" + result + "'";
            return result;
        } else if (value.getValue() instanceof Double) {
            return value.getValue().toString();
        } else if (value.getValue() instanceof Integer) {
            return value.getValue().toString();
        } else if (value.getValue() instanceof Boolean) {
            return ((Boolean) value.getValue()) ? "1" : "0";
        } else if (value.getValue() instanceof String) {
            return "'" + value.getValue() + "'";
        } else {
            return (String) value.getValue();
        }
    }

    public static String translateValue(Object value) {
        if (value instanceof Timestamp) {
            Date date = new Date();
            date.setTime(((Timestamp) value).getTime());
            return new SimpleDateFormat(DATE_FORMAT).format(date);
        } else if (value instanceof Date) {
            return new SimpleDateFormat(DATE_FORMAT).format(value);
        } else if (value instanceof Double) {
            return value.toString();
        } else if (value instanceof BigDecimal) {
            return value.toString();
        } else if (value instanceof Integer) {
            return value.toString();
        } else if (value instanceof Long) {
            return value.toString();
        } else {
            if (value != null) {
                return escapeForDatabase((value.toString()));
            } else return null;
        }
    }

    private static String escapeForDatabase(String text) {
        if (text != null) {
            return text.replaceAll("'", "''");
        }
        return null;
    }
}
