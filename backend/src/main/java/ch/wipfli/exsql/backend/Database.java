package ch.wipfli.exsql.backend;

import org.apache.poi.ss.usermodel.Sheet;

import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Database {

    private final static Logger LOGGER = Logger.getGlobal();

    private static final String DB_USER = "";
    private static final String DB_PASSWORD = "";

    private static Connection getConnection(String db) throws SQLException {
        return DriverManager.getConnection("jdbc:h2:~/"+db+";DB_CLOSE_ON_EXIT=FALSE", DB_USER, DB_PASSWORD);
    }

    protected static void executeStatement(String statement, String databaseName) {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            //do nothing
        }
        try (Connection con = getConnection(databaseName)) {
            Statement st = con.createStatement();
            st.execute(statement);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "", e);
            e.printStackTrace();
        }
    }

    protected static List<List<ValueModel>> selectStatement(String statement, String databaseName) throws SQLException {
        LOGGER.info(statement);

        final List<List<ValueModel>> list = new ArrayList<>();
        try (Connection con = getConnection(databaseName)) {
            final Statement st = con.createStatement();
            final ResultSet rs = st.executeQuery(statement);

            final ResultSetMetaData rsmd = rs.getMetaData();
            int cols = rsmd.getColumnCount();

            final List<ValueModel> valueList = new ArrayList<>();

            for (int i = 1; i <= cols; i++) {
                String colName = rsmd.getColumnName(i);
                int colType = rsmd.getColumnType(i);
                valueList.add(new ValueModel<>(colName, colType, colName.getClass()));
                LOGGER.fine(MessageFormat.format("ColumnName: {0} ColumnType: {1}", colName, colType));
            }
            list.add(valueList);

            while (rs.next()) {
                final List<ValueModel> values = new ArrayList<>();
                for (int i = 1; i <= cols; i++) {
                    String colName = rsmd.getColumnName(i);
                    int colType = rsmd.getColumnType(i);
                    Object value = rs.getObject(colName);
                    if (value != null) {
                        values.add(new ValueModel<>(value, colType, value.getClass()));
                        LOGGER.fine(MessageFormat.format("Column: {0} Row: {1} Value: {2}", i, rs.getRow(), value));
                    } else {
                        values.add(new ValueModel<>(value, colType, String.class));
                        LOGGER.fine(MessageFormat.format("Column: {0} Row: {1} Value: {2}", i, rs.getRow(), value));
                    }
                }
                list.add(values);
            }
        }
        return list;
    }

    protected static SheetModel selectStatement(String statement, String sheetName, String databaseName) throws SQLException {
        LOGGER.info(statement);

        final SheetModel sheet = new SheetModel();
        sheet.setName(sheetName);

        try (Connection con = getConnection(databaseName)) {
            final Statement st = con.createStatement();
            final ResultSet rs = st.executeQuery(statement);

            final ResultSetMetaData rsmd = rs.getMetaData();
            int cols = rsmd.getColumnCount();

            for (int i = 1; i <= cols; i++) {
                String colName = rsmd.getColumnName(i);
                int colType = rsmd.getColumnType(i);
                sheet.getColumns().add(new ValueModel<>(colName, colType, colName.getClass()));
                LOGGER.fine(MessageFormat.format("ColumnName: {0} ColumnType: {1}", colName, colType));
            }

            while (rs.next()) {
                final List<String> values = new ArrayList<>();
                for (int i = 1; i <= cols; i++) {
                    String colName = rsmd.getColumnName(i);
                    Object value = rs.getObject(colName);
                    if (value != null) {
                        values.add(DatatypeMapper.translateValue(value));
                        LOGGER.fine(MessageFormat.format("Column: {0} Row: {1} Value: {2}", i, rs.getRow(), value));
                    } else {
                        values.add(null);
                        LOGGER.fine(MessageFormat.format("Column: {0} Row: {1} Value: {2}", i, rs.getRow(), value));
                    }
                }
                sheet.getContent().add(values);
            }
        }
        return sheet;
    }
}
