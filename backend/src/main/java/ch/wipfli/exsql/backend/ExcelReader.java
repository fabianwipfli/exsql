package ch.wipfli.exsql.backend;

import java.io.*;
import java.math.BigDecimal;
import java.sql.Types;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.*;

import javax.xml.crypto.Data;

import static java.sql.Types.*;

public class ExcelReader {

    private final static Logger LOGGER = Logger.getGlobal();

    protected List<List<ValueModel>> importCSV(File file, String encoding, String delimiter) throws IOException {
        final List<List<ValueModel>> records = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding))) {
            String line;
            while ((line = br.readLine()) != null) {
                final String[] values = line.split(delimiter);
                final List<ValueModel> items = new ArrayList<>();
                for (String value : values) {
                    items.add(new ValueModel<>(value, VARCHAR, value.getClass()));
                }
                records.add(items);
            }
        }
        return records;
    }

    protected List<List<List<ValueModel>>> importExcel(File file, int[] headerPosition) throws IOException {
        final List<List<List<ValueModel>>> resultList = new ArrayList<>();
        try (Workbook workbook = WorkbookFactory.create(file)) {
            int numOfSheet = 0;
            for (Sheet sheet : workbook) {
                //Remove rows till header position
                int startRowNum = 0;
                if (headerPosition.length > 0) {
                    for (int i = 0; i < headerPosition[numOfSheet] - 1; i++) {
                        sheet.removeRow(sheet.getRow(i));
                    }
                    startRowNum = headerPosition[numOfSheet] - 1;
                    numOfSheet++;
                }

                final List<List<ValueModel>> resultSheetList = new ArrayList<>();
                final List<ValueModel> columns = getColumns(sheet, startRowNum);

                //Check if has columns
                if (columns.contains(null) || columns.size() == 0) {
                    LOGGER.severe(MessageFormat.format("Skip sheet \"{1}\". Columns are empty.", sheet.getSheetName()));
                } else {
                    for (Row row : sheet) {
                        final List<ValueModel> resultRowList = new ArrayList<>();
                        if (row.getRowNum() == startRowNum) {
                            resultSheetList.add(columns);
                        } else {
                            for (int i = 0; i < columns.size(); i++) {
                                final Cell cell = row.getCell(i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                                final ValueModel column = columns.get(i);
                                if (cell == null) {
                                    resultRowList.add(new ValueModel<>(null, column.getDatabaseType(), column.getClassType()));
                                } else {
                                    switch (column.getDatabaseType()) {
                                        case TIMESTAMP:
                                            resultRowList.add(new ValueModel<>(cell.getDateCellValue(), TIMESTAMP, column.getClassType()));
                                            break;
                                        case Types.NUMERIC:
                                            if (column.getClassType() == Boolean.class) {
                                                resultRowList.add(new ValueModel<>(cell.getBooleanCellValue() ? 1 : 0, Types.NUMERIC, column.getClassType()));
                                            } else {
                                                resultRowList.add(new ValueModel<>(cell.getNumericCellValue(), Types.NUMERIC, column.getClassType()));
                                            }
                                            break;
                                        default:
                                            resultRowList.add(new ValueModel<>(DatatypeMapper.translateValue(DatatypeMapper.getValue(cell)), VARCHAR, column.getClassType()));
                                    }
                                }
                            }
                            resultSheetList.add(resultRowList);
                        }
                    }
                    resultList.add(resultSheetList);
                }
            }
        }

        return resultList;
    }

    protected List<SheetModel> importExcel(File file, String name, int nameStartNumber, int[] headerPosition, String sessionKey) throws IOException {
        final List<SheetModel> resultList = new ArrayList<>();
        try (Workbook workbook = WorkbookFactory.create(file)) {
            int numOfSheet = 0;
            for (Sheet sheet : workbook) {
                //Remove rows till header position
                int startRowNum = 0;
                if (headerPosition.length > numOfSheet) {
                    for (int i = 0; i < headerPosition[numOfSheet] - 1; i++) {
                        sheet.removeRow(sheet.getRow(i));
                    }
                    startRowNum = headerPosition[numOfSheet] - 1;
                }
                numOfSheet++;

                SheetModel sheetModel = new SheetModel();
                sheetModel.setName(name + ++nameStartNumber);
                final List<ValueModel> columns = getColumns(sheet, startRowNum);
                sheetModel.getColumns().addAll(columns);

                for (Row row : sheet) {
                    final List<String> resultRowList = new ArrayList<>();
                    if (row.getRowNum() != startRowNum) {
                        for (int i = 0; i < columns.size(); i++) {
                            final Cell cell = row.getCell(i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                            final ValueModel column = columns.get(i);
                            if (cell == null) {
                                resultRowList.add(null);
                            } else {
                                switch (column.getDatabaseType()) {
                                    case TIMESTAMP:
                                        resultRowList.add(DatatypeMapper.translateValue(cell.getDateCellValue()));
                                        break;
                                    case Types.NUMERIC:
                                        if (column.getClassType() == Boolean.class) {
                                            resultRowList.add(DatatypeMapper.translateValue(cell.getBooleanCellValue() ? 1 : 0));
                                        } else {
                                            resultRowList.add(DatatypeMapper.translateValue(cell.getNumericCellValue()));
                                        }
                                        break;
                                    default:
                                        resultRowList.add(DatatypeMapper.translateValue(DatatypeMapper.translateValue(DatatypeMapper.getValue(cell))));
                                }
                            }
                        }
                        sheetModel.getContent().add(resultRowList);
                    }
                }
                resultList.add(sheetModel);
            }
        }

        return resultList;
    }

    private List<ValueModel> getColumns(Sheet sheet, int rowStartNum) {
        final List<ValueModel> firstRow = new ArrayList<>();

        for (Row row : sheet) {
            if (row.getRowNum() == rowStartNum) {
                final Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    final Cell cell = cellIterator.next();
                    firstRow.add(new ValueModel<>(new DataFormatter().formatCellValue(cell), -1, null));
                }
            } else if (row.getRowNum() < rowStartNum) {
                //skip till start row.
            } else {
                for (int i = 0; i < firstRow.size(); i++) {
                    final Cell cell = row.getCell(i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                    final ValueModel column = firstRow.get(i);
                    final ValueModel newColumn = new ValueModel<>(column.getValue(), -1, null);

                    if (cell == null) {
                        firstRow.get(i).setDatabaseType(VARCHAR);
                        firstRow.get(i).setClassType(String.class);
                    } else {
                        newColumn.setDatabaseType(DatatypeMapper.getDatabaseType(cell));
                        newColumn.setClassType(DatatypeMapper.getValue(cell).getClass());
                    }

                    if (column.getClassType() == null) {
                        column.setDatabaseType(newColumn.getDatabaseType());
                        column.setClassType(newColumn.getClassType());
                    } else if (column.getClassType() != newColumn.getClassType()) {
                        column.setDatabaseType(VARCHAR);
                        column.setClassType(String.class);
                    }
                }
            }
        }
        return firstRow;
    }

    public String[] getSheets(File file) throws IOException {
        try (Workbook workbook = WorkbookFactory.create(file)) {
            String[] sheets = new String[workbook.getNumberOfSheets()];
            int index = 0;
            for (Sheet sheet : workbook) {
                sheets[index] = sheet.getSheetName();
                index++;
            }
            return sheets;
        }
    }
}
