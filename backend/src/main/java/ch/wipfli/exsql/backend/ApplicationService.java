package ch.wipfli.exsql.backend;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface ApplicationService {

    List<SheetModel> test() throws IOException, SQLException;

    List<List<ValueModel>> select(String statement) throws IOException, SQLException;

    List<List<ValueModel>> select(String statement, String sessionKey) throws IOException, SQLException;

    List<List<List<ValueModel>>>  selectMultiple(String statement, String sessionKey) throws SQLException;

    List<SheetModel> selectExt(String statement, String sessionKey) throws IOException, SQLException;

    void importExcel(File file, String name, int nameStartNumber, int[] headerPosition ,BiConsumer<List<ValueModel>, String> callback) throws IOException, SQLException;

    List<SheetModel> importExcel(File file, String tableName, int[] headerPosition, String sessionKey) throws IOException, SQLException;

    void importCSV(File file, String tablename, String encoding, String delimiter, BiConsumer<List<ValueModel>, String> callback) throws IOException;

    List<List<ValueModel>> importCSV(File file, String tableName, String encoding, String delimiter) throws SQLException;

    List<SheetModel> importCSVExt(File file, String tableName, String encoding, String delimiter, String sessionKey) throws SQLException;

    void exportExcel(List<List<ValueModel>> list, File file) throws IOException;

    void exportCSV(List<List<ValueModel>> list, File file, String encoding, String delimiter) throws IOException;

    void exportExcelMultiple(List<List<List<ValueModel>>> list, File file) throws IOException;

    List<SheetModel>  remove(String tableName, String sessionKey) throws SQLException;

    List<SheetModel>  remove(String tableName) throws SQLException;

    String[] getSheets(File file) throws IOException;

    List<SheetModel> getSheets(String sesssionKey) throws SQLException;
}
