package ch.wipfli.exsql.backend;

import java.lang.reflect.Field;
import java.sql.Types;

public class ValueModel<T> {

    private T value;
    private int databaseType;
    private Class classType;

    public ValueModel(T value, int databaseType, Class classType)
    {
        this.value = value;
        this.databaseType = databaseType;
        this.classType = classType;
    }

    public T getValue() {
        return value;
    }

    public String getStringValue()
    {
        return DatatypeMapper.translateValue(value);
    }

    public void setValue(T value) {
        this.value = value;
    }

    public int getDatabaseType() {
        return databaseType;
    }

    public void setDatabaseType(int databaseType) {
        this.databaseType = databaseType;
    }

    public String getDatabaseTypeName() {
        return translateDatabaseType(databaseType);
    }

    public Class getClassType() {
        return classType;
    }

    public void setClassType(Class classType) {
        this.classType = classType;
    }

    private String translateDatabaseType(int i) {
        for (Field field : Types.class.getFields()) {
            try {
                if ((Integer)field.get(null) == i) {
                    return field.getName();
                }
            }
            catch(Exception e)
            {
                //do nothing
            }
        }
        return null;
    }
}
