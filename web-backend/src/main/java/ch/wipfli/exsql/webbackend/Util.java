package ch.wipfli.exsql.webbackend;

public class Util {
    public static int parseInt(String number, int defaultVal) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return defaultVal;
        }
    }
}
