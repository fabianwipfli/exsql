package ch.wipfli.exsql.webbackend.controller;

import ch.wipfli.exsql.backend.ApplicationService;
import ch.wipfli.exsql.backend.ApplicationServiceImpl;
import ch.wipfli.exsql.backend.SheetModel;
import ch.wipfli.exsql.backend.ValueModel;
import ch.wipfli.exsql.webbackend.Util;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/service")
class ServiceController {

    private static final Logger logger = LoggerFactory.getLogger(ServiceController.class);


    @GetMapping("/select")
    public List<SheetModel> select(@RequestParam("statement") String statement, @RequestParam("session_key") String sessionKey) throws SQLException {
        final List<SheetModel> sheets = new ArrayList<>();
        final ApplicationService service = new ApplicationServiceImpl();
        try {
            sheets.addAll(service.selectExt(statement, sessionKey));
        } catch (IOException ie) {
            ie.printStackTrace();
        }
        return sheets;
    }

    @GetMapping("/sheets")
    public List<SheetModel> getSheets(@RequestParam("session_key") String sessionKey) {
        final List<SheetModel> sheets = new ArrayList<>();
        final ApplicationService service = new ApplicationServiceImpl();
        try {
            sheets.addAll(service.getSheets(sessionKey));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sheets;
    }

    @GetMapping("/exportCsv")
    public @ResponseBody
    byte[] getExportCsv(@RequestParam("statement") String statement, @RequestParam("encoding") String encoding, @RequestParam("delimitter") String delimitter, @RequestParam("session_key") String sessionKey) {
        final ApplicationService service = new ApplicationServiceImpl();
        try {
            java.io.File file = new java.io.File(UUID.randomUUID() + ".csv");
            List<List<ValueModel>> sheet = service.select(statement, sessionKey);
            service.exportCSV(sheet, file, encoding, delimitter);
            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
            byte[] result = IOUtils.toByteArray(resource.getInputStream());
            file.delete();
            logger.info(MessageFormat.format("Delete {0}", file.getAbsolutePath()));
            return result;
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    @GetMapping("/exportExcel")
    public @ResponseBody
    byte[] getExportCsv(@RequestParam("statement") String statement, @RequestParam("session_key") String sessionKey) {
        final ApplicationService service = new ApplicationServiceImpl();
        try {
            java.io.File file = new java.io.File(UUID.randomUUID() + ".xslx");
            List<List<List<ValueModel>>> sheets = service.selectMultiple(statement, sessionKey);
            service.exportExcelMultiple(sheets, file);
            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
            byte[] result = IOUtils.toByteArray(resource.getInputStream());
            file.delete();
            logger.info(MessageFormat.format("Delete {0}", file.getAbsolutePath()));
            return result;
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }


    @PostMapping("/uploadCsv")
    public List<SheetModel> uploadCsv(@RequestParam("file") MultipartFile file, @RequestParam("sheetName") String sheetName, @RequestParam("encoding") String encoding, @RequestParam("delimiter") String delimiter, @RequestParam("session_key") String sessionKey) {
        final List<SheetModel> sheets = new ArrayList<>();
        final ApplicationService service = new ApplicationServiceImpl();

        try {
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get("" + file.getOriginalFilename());
            Files.write(path, bytes);

            sheets.addAll(service.importCSVExt(path.toFile(), sheetName, encoding, delimiter, sessionKey));
            path.toFile().delete();
            logger.info(MessageFormat.format("Delete {0}", path.toFile().getAbsolutePath()));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }

        return sheets;
    }

    @PostMapping("/uploadExcel")
    public List<SheetModel> uploadExcel(@RequestParam("file") MultipartFile file, @RequestParam("name") String name, @RequestParam("headerPosition") String headerPosition, @RequestParam("session_key") String sessionKey) {
        final List<SheetModel> sheets = new ArrayList<>();
        final ApplicationService service = new ApplicationServiceImpl();

        try {
            String[] headerPositionArr = headerPosition.split(",");
            int[] headerPositionArrInt = Arrays.stream(headerPositionArr).mapToInt((i) -> Util.parseInt(i, 1)).toArray();
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get("" + file.getOriginalFilename());
            Files.write(path, bytes);
            path.toFile().delete();
            logger.info(MessageFormat.format("Delete {0}", path.toFile().getAbsolutePath()));
            sheets.addAll(service.importExcel(path.toFile(), name, headerPositionArrInt, sessionKey));

        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }

        return sheets;
    }

    @DeleteMapping("/{sheetName}")
    public List<SheetModel> removeSheet(@PathVariable("sheetName") String sheetName, @RequestParam("session_key") String sessionKey) {
        final ApplicationService service = new ApplicationServiceImpl();
        final List<SheetModel> sheets = new ArrayList<>();
        try {
            sheets.addAll(service.remove(sheetName, sessionKey));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sheets;
    }
}
